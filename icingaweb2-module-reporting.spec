# Reporting Module for Icinga Web 2 | (c) 2018-2019 Icinga Development Team <info@icinga.com> | GPLv2+

%global revision 1
%global module_name reporting

%global icingaweb_min_version 2.6.0

Name:           icingaweb2-module-%{module_name}
Version:        0.9.1
Release:        %{revision}%{?dist}
Summary:        Reporting - Icinga Web 2 module
Group:          Applications/System
License:        GPLv2+
URL:            https://icinga.com
Source0:        https://github.com/Icinga/icingaweb2-module-%{module_name}/archive/v%{version}.tar.gz
Source1:        %{service_name}.service
#/icingaweb2-module-%{module_name}-%{version}.tar.gz
BuildArch:      noarch

%global basedir %{_datadir}/icingaweb2/modules/%{module_name}
%global service_name icinga-%{module_name}

%if "%{_vendor}" == "suse"
%global service_user wwwrun
%else # suse
%global service_user apache
%endif # suse

BuildRequires:  systemd-devel
Requires:       systemd

Requires:       icingaweb2 >= %{icingaweb_min_version}
Requires:       php-Icinga >= %{icingaweb_min_version}

Requires:       icingaweb2-module-reactbundle >= 0.4
Requires:       icingaweb2-module-ipl >= 0.2.1
Requires:       icingaweb2-module-pdfexport >= 0.9

%description
Icinga Reporting is the central component for reporting related functionality
in the monitoring web frontend and framework Icinga Web 2. The engine allows
you to create reports over a specified time period for ad-hoc and scheduled
generation of reports. Other modules use the provided functionality in order
to provide concrete reports.

If you are looking for SLA reports for your hosts and services, please also
install the idoreports module.

%prep
%setup -q
#-n icingaweb2-module-%{module_name}-%{version}

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{basedir}

cp -r * %{buildroot}%{basedir}

install -d %{buildroot}%{_unitdir}
install -m 0644 %{SOURCE1} %{buildroot}%{_unitdir}/%{service_name}.service

# Replace user in service unit
sed -i -e 's~^User=.*~User=%{service_user}~' %{buildroot}%{_unitdir}/%{service_name}.service

%if "%{_vendor}" == "suse"
install -d %{buildroot}%{_sbindir}
ln -sf /usr/sbin/service %{buildroot}%{_sbindir}/rc%{service_name}
%endif # suse

%clean
rm -rf %{buildroot}

%pre
%if "%{_vendor}" == "suse"
  %service_add_pre %{service_name}.service
%endif # suse

exit 0

%post
set -e

%if "%{_vendor}" == "suse"
  %service_add_post %{service_name}.service
%else # suse
%systemd_post %{service_name}.service
%endif # suse

exit 0

%preun
set -e

%if "%{_vendor}" == "suse"
  %service_del_preun %{service_name}.service
%else # suse
  %systemd_preun %{service_name}.service
%endif # suse

# Only for removal
if [ $1 == 0 ]; then
    echo "Disabling icingaweb2 module '%{module_name}'"
    rm -f /etc/icingaweb2/enabledModules/%{module_name}
fi

exit 0

%postun
set -e

%if "%{_vendor}" == "suse"
  %service_del_postun %{service_name}.service
%else # suse
  %systemd_postun_with_restart %{service_name}.service
%endif # suse

exit 0

%files
%doc README.md LICENSE

%defattr(-,root,root)
%{basedir}

%{_unitdir}/%{service_name}.service

%if "%{_vendor}" == "suse"
%{_sbindir}/rc%{service_name}
%endif # suse

%changelog
* Fri Aug 30 2019 Markus Frosch <markus.frosch@icinga.com> - 0.9.1-1
- Initial package version
